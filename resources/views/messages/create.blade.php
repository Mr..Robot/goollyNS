@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Identifires/add

                    </div>

                    <div class="panel-body">
                                                @include('flash::message')
                        <form role="form" action="/messages/send" method="POST">
                            {{ csrf_field() }}
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="msg">Message Body</label>
                                        <textarea  rows="4" class="form-control"  name="msg"  id="msg" placeholder="Write your message" required>{{ old('msg') }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Type</label>
                                            <select class="form-control select2" name="sendon" style="width: 100%;">
                                                @foreach($types as $type)
                                                    <option value="{{$type->name}}">{{$type->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <label for="Identifire">Phone, Email,ICASH, etc..</label>
                                            <input type="text" class="form-control"  name="identifier"  id="Identifire" placeholder="Enter Identifire" required>
                                        </div>
                                    </div>
                                </div>

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
