@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Messages
                    <div class="panel-body">
                        <div class="box-body no-padding">
                            <table class="table table-striped">
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th style="width: 180px">From</th>
                                    <th>Message</th>
                                    <th style="width: 140px">Date</th>
                                </tr>
                                @foreach($msgs as $k=>$msg)
                                    <tr>
                                        <td>{{$k+1}}</td>
                                        <td>{{$msg->organization->name}}</td>
                                        <td>{{$msg->body}}</td>
                                        <td>{{$msg->created_at->diffforHumans()}}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
