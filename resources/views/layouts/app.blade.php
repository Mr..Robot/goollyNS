<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title><!-- {{ config('app.name', ' -->GoollyNS<!-- ') }} --></title>

    <!-- Styles -->
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/AdminLTE.min.css') }}" rel="stylesheet">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />

</head>
<body>
<div id="app">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                   <!-- {{ config('app.name', ' -->GoollyNS<!-- ') }} -->
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    &nbsp;
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ route('login') }}">Login</a></li>
                        <li><a href="{{ route('register') }}">Register</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                Requests
                                <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                @foreach($requests as $k=>$request)
                                    @if($k>=1)
                                        <li role="separator" class="divider"></li>
                                    @endif
                                    <li><a href="/organizations/{{$request->organization->name}}"><b><i>{{$request->organization->name}}</i></b> wants To contact you</a></li>
                                @endforeach


                            </ul>
                        </li>

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="GET" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

        <div class="row">
            @if (Auth::guest())
            @else
                <div class=" col-md-2">
                    <div class="list-group">
                        <a href="#" class="list-group-item active">
                            Menu
                        </a>
                        <a href="/messages" class="list-group-item">Messages</a>
                        <a href="/organizations" class="list-group-item">Organizations</a>
                        <a href="/identifiers" class="list-group-item">Identifiers</a>
                        <a href="/devices" class="list-group-item">Devices</a>
                        <a href="/subscriptions" class="list-group-item">Subscriptions</a>
                        <a href="/blacklist" class="list-group-item">BlackList</a>
                        <a href="/myorgs" class="list-group-item">My Organizations</a>
                    </div>
                </div>
            @endif
            <div class=" col-md-10">
            @yield('content')
            </div>
        </div>

</div>

<!-- Scripts -->
<script src="{{ asset('js/app.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>

<script type="text/javascript">
    
    $('.dropdown-toggle').dropdown()
</script>


</body>
</html>
