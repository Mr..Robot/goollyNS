<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:mc="http://www.w3.org/1999/xhtml">
<head>
    <!-- Define Charset -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <!-- Responsive Meta Tag -->
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;" />
    <title>Lancer - Responsive Email Template</title><!-- Responsive Styles and Valid Styles -->

    <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,700' rel='stylesheet' type='text/css'>
    <style type="text/css">

        body{
            width: 100%;
            background-color: #ecf0f1;
            margin:0;
            padding:0;
            -webkit-font-smoothing: antialiased;
        }

        p,h1,h2,h3,h4{
            margin-top:0;
            margin-bottom:0;
            padding-top:0;
            padding-bottom:0;
        }

        span.preheader{display: none; font-size: 1px;}

        html{
            width: 100%;
        }

        table{
            font-size: 14px;
            border: 0;
            border-collapse:collapse;
            mso-table-lspace:0pt;
            mso-table-rspace:0pt;
        }

        /* ----------- responsivity ----------- */
        @media only screen and (max-width: 640px){
            /*------ top header ------ */
            .rounded-edg-bg{width: 440px !important; height: 10px !important;}
            .main-header{line-height: 28px !important; font-size: 17px !important;}
            .subheader{width: 390px !important;}
            .main-subheader{line-height: 28px !important;}

            /*----- main image -------*/
            .main-image img{width: 420px !important; height: auto !important;}

            /*-------- container --------*/
            .container580{width: 440px !important;}
            .container560{width: 420px !important;}
            .main-content{width: 438px !important;}
            /*-------- divider --------*/
            .divider img{width: 438px !important; height: 10px !important;}

            /*-------- secions ----------*/
            .section-item{width: 420px !important;}
            .table-inside{width: 418px !important;}
            .section-img img{width: 420px !important; height: auto !important;}
            .table-rounded-edg-bg{width: 420px !important; height: 5px !important;}

            /*-------- cta ------------*/

        }

        @media only screen and (max-width: 479px){

            /*------ top header ------ */
            .rounded-edg-bg{width: 280px !important; height: 10px !important;}
            .main-header{line-height: 28px !important; font-size: 17px !important;}
            .subheader{width: 260px !important;}
            .main-subheader{line-height: 28px !important;}

            /*----- main image -------*/
            .main-image img{width: 260px !important; height: auto !important;}

            /*-------- container --------*/
            .container580{width: 280px !important;}
            .container560{width: 260px !important;}
            .main-content{width: 278px !important;}
            /*-------- divider --------*/
            .divider img{width: 278px !important; height: 10px !important;}

            /*-------- secions ----------*/
            .section-item{width: 260px !important;}
            .table-inside{width: 258px !important;}
            .section-img img{width: 260px !important; height: auto !important;}
            .table-rounded-edg-bg{width: 260px !important; height: 5px !important;}

            /*-------- cta ----------*/
            .icon img{width: 20px !important; height: 20px !important;}
            .cta-text{text-align: center !important; line-height: 24px !important;}

            /*-------- footer ------------*/


        }

    </style>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="ecf0f1">
    <tr>
        <td align="center"  bgcolor="ffffff">
            <table width="580" cellpadding="0" cellspacing="0" border="0" class="container580">
                <tr><td height="20"></td></tr>
                <tr>
                    <td>
                        <table border="0" align="left" cellpadding="0" cellspacing="0" class="logo">
                            <tr>
                                <td align="center">
                                    <a href="#" style="display: block; border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="logo" width="128" height="30" border="0" style="display: block; width: 128px; height: 30px" src="{{ asset('img/logo.png') }}" alt="logo" /></a>
                                </td>
                            </tr>
                        </table>
                        <table border="0" align="left" cellpadding="0" cellspacing="0" class="separator">
                            <tr>
                                <td height="20" width="20"></td>
                            </tr>
                        </table>
                        <table border="0" align="right" cellpadding="0" cellspacing="0" class="socials">
                            <tr>
                                <td align="center" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;">
                                    <table border="0" align="center" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <a style="display: block; width: 24px; border-style: none !important; border: 0 !important;" href="#"><img editable="true" mc:edit="google" width="24" height="24" border="0" style="display: block;" src="{{ asset('img/google.png') }}" alt="google" /></a>
                                            </td>
                                            <td>&nbsp;&nbsp;&nbsp;</td>
                                            <td>
                                                <a style="display: block; width: 24px; border-style: none !important; border: 0 !important;" href="#"><img editable="true" mc:edit="facebook" width="24" height="24" border="0" style="display: block;" src="{{ asset('img/facebook.png') }}" alt="facebook" /></a>
                                            </td>
                                            <td>&nbsp;&nbsp;&nbsp;</td>
                                            <td>
                                                <a style="display: block; width: 24px; border-style: none !important; border: 0 !important;" href="#"><img editable="true" mc:edit="twitter" width="24" height="24" border="0" style="display: block;" src="{{ asset('img/twitter.png') }}" alt="twitter" /></a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><td height="20"></td></tr>
            </table>
        </td>
    </tr>
    <tr bgcolor="e6e9ea"><td height="2"></td></tr>
    <tr><td height="40"></td></tr>
    <tr>
        <td align="center">
            <img editable="true" src="{{ asset('img/top-rounded-bg.png') }}" style="display: block; width: 580px; height: 10px; line-height: 1px;" width="580" height="10" border="0" class="rounded-edg-bg" />
        </td>
    </tr>
    <tr mc:repeatable>
        <td align="center">
            <table border="0" align="center" width="580" cellpadding="0" cellspacing="0" bgcolor="e6e9ea" class="container580">
                <tr>
                    <td align="center">
                        <table border="0" align="center" width="578" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="main-content">
                            <tr>
                                <td align="center">
                                    <table width="560" cellpadding="0" align="center" cellspacing="0" border="0" class="container560">
                                        <tr>
                                            <td align="center" class="main-image">
                                                <img editable="true" mc:edit="main-image" src="{{ asset('img/main-image.png') }}" style="display: block; width: 560px; height: 300px; line-height: 1px;" width="560" height="300" border="0" alt="main image" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr mc:repeatable>
        <td>
            <table width="580" cellpadding="0" align="center" cellspacing="0" bgcolor="e6e9ea" border="0" class="container580">
                <tr>
                    <td>
                        <table border="0" align="center" width="578" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="main-content">
                            <tr><td bgcolor="ffffff" height="35"></td></tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr mc:repeatable>
        <td align="center">
            <table border="0" align="center" width="580" cellpadding="0" cellspacing="0" bgcolor="e6e9ea" class="container580">
                <tr>
                    <td>
                        <table border="0" align="center" width="578" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="main-content">

                            <tr mc:repeatable >
                                <td align="center">
                                    <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container560">
                                        <tr>
                                            <td align="center" mc:edit="main-header" style="color: #424345; font-size: 22px; font-weight: bold; font-family: 'Roboto Slab', Arial, sans-serif;" class="main-header">
                                                <multiline>
                                                    You need To Activate your Eamil
                                                </multiline>
                                            </td>
                                        </tr>
                                        <tr><td height="20"></td></tr>
                                        <tr>
                                            <td align="center" mc:edit="main-subtitle" style="color: #95a5a6; font-size: 14px; font-family: 'Roboto', Arial, sans-serif;" class="main-header">
                                                <multiline>
                                                    This Is your verification code : <b> {{$Xcode}} </b>
                                                </multiline>
                                            </td>
                                        </tr>
                                        <tr><td height="15"></td></tr>
                                        <tr>
                                            <td align="center">
                                                <table border="0" width="400" align="center" cellpadding="0" cellspacing="0" class="subheader">
                                                    <tr>
                                                        <td align="center" mc:edit="main-subheader" style="color: #bdc3c7; font-size: 14px; font-weight: 300; font-family: 'Roboto', Arial, sans-serif; line-height: 28px;" class="main-subheader">
                                                            <multiline>
                                                                Please Entere this code in the confirmatin page!!<br>
                                                                or just click <a href="http://goollyns.dev/activate/{{$email}}/{{$Xcode}}" style="color: red;">Verify</a>

                                                            </multiline>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr><td height="25"></td></tr>

                                        <tr>
                                            <td align="center">
                                                <table border="0" width="230" align="center" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td align="left">
                                                            <a href="#" style="display: block; width: 120px; border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="download" width="120" height="32" border="0" style="display: block; width: 120px; height: 32px;" src="{{ asset('img/download.png') }}" alt="download" /></a>
                                                        </td>
                                                        <td align="right">
                                                            <a href="#" style="display: block; width: 90px; border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="free-trial" width="90" height="32" border="0" style="display: block; width: 90px; height: 32px;" src="{{ asset('img/free-trial.png') }}" alt="download" /></a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr mc:repeatable>
        <td>
            <table width="580" cellpadding="0" align="center" cellspacing="0" bgcolor="e6e9ea" border="0" class="container580">
                <tr>
                    <td>
                        <table border="0" align="center" width="578" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="main-content">
                            <tr><td bgcolor="ffffff" height="35"></td></tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center">
            <img editable="true" src="{{ asset('img/bottom-rounded-bg.png') }}" style="display: block; width: 580px; height: 10px; line-height: 1px;" width="580" height="10" border="0" class="rounded-edg-bg" />
        </td>
    </tr>
    <tr><td height="40"></td></tr>
    <tr>
        <td align="center"  bgcolor="1e2f3e">
            <table width="580" cellpadding="0" cellspacing="0" border="0" class="container580">
                <tr><td height="10"></td></tr>
                <tr>
                    <td>
                        <table border="0" align="left" cellpadding="0" cellspacing="0" class="copy">
                            <tr>
                                <td align="center" mc:edit="copy" style="color: #ffffff; font-size: 12px; font-weight: 300; font-family: 'Roboto', Arial, sans-serif; line-height: 28px;" >
                                    <multiline>
                                        UNS <span style="color: #9aa3ab;">© Copyright 2017</span>
                                    </multiline>
                                </td>
                            </tr>
                        </table>
                        <table border="0" align="left" cellpadding="0" cellspacing="0" class="separator">
                            <tr>
                                <td height="20" width="20"></td>
                            </tr>
                        </table>
                        <table border="0" align="right" cellpadding="0" cellspacing="0" class="socials">
                            <tr>
                                <td align="center" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;">
                                    <table border="0" align="center" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <a style="display: block; width: 24px; border-style: none !important; border: 0 !important;" href="#"><img editable="true" mc:edit="google" width="24" height="24" border="0" style="display: block;" src="{{ asset('img/google.png') }}" alt="google" /></a>
                                            </td>
                                            <td>&nbsp;&nbsp;&nbsp;</td>
                                            <td>
                                                <a style="display: block; width: 24px; border-style: none !important; border: 0 !important;" href="#"><img editable="true" mc:edit="facebook" width="24" height="24" border="0" style="display: block;" src="{{ asset('img/facebook.png') }}" alt="facebook" /></a>
                                            </td>
                                            <td>&nbsp;&nbsp;&nbsp;</td>
                                            <td>
                                                <a style="display: block; width: 24px; border-style: none !important; border: 0 !important;" href="#"><img editable="true" mc:edit="twitter" width="24" height="24" border="0" style="display: block;" src="{{ asset('img/twitter.png') }}" alt="twitter" /></a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><td height="5"></td></tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>