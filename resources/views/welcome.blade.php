<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/logout') }}">Logout</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Register</a>
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Goolly NS                
                </div>

                <div class="links">
                @if (Route::has('login'))

                        @auth

                            <a href="/messages">Messages</a>
                            <a href="/organizations">Organizations</a>
                            <a href="/identifiers">Identifiers</a>
                            <a href="/devices">Devices</a>
                            <a href="/blacklist">Blacklist</a>
                            <a href="/subscriptions">Subscriptions</a><br>
                            <a href="/organizations/login">Send Message : please logout then login as ORG click here</a>

                            @else
                                <a href="{{ route('register') }}">Register New user</a>
                                <a href="{{ route('login') }}">Login as : user</a><br>
                                <a href="/organizations/login">login as : organization</a>

                                <a href="/organizations/create">register as : organization</a>

                                
                                @endauth

                @endif
                </div>
                            </div>
            </div>
        </div>
    </body>
</html>
