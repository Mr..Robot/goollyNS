@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Organizations
                    <div class="pull-right">
                        <a href="/organizations/create"> <span class="label label-primary">New
                            <i class="fa fa-plus"></i></span></a></div></div>
                    <div class="panel-body">
                        <div class="box-body no-padding">
                            <table class="table table-striped">
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th style="width: 180px">Name</th>
                                    <th>Logo</th>
                                    <th class="pull-right">Action</th>
                                </tr>
                                @foreach($organizations as $k=>$org)
                                <tr>
                                    <td>{{$k+1}}</td>
                                    <td><a href="/organizations/{{$org->name}}">{{$org->name}}</a></td>
                                    <td>{{$org->logo}}</td>
                                    @if(Auth::check())
                                    <td class="pull-right">                                                 
                                        @if(Auth::User()->myOrgs()->get()->contains($org))
                                            
                                                @if(Auth::guard('org')->user() == $org)
                                                    <a href="/messages/send">Send Message</a>
                                                @else
                                                    <a href="/organizations/login">Login</a>
                                                @endif
                                        @else
                                            <a href="/organizations/{{$org->name}}">Visit</a>

                                        @endif
                                        </td>
                                    @endif
                                </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                
            </div>
        </div>
    </div>
    @endsection
