@extends('layouts.app')

@section('content')
    <div class="container">
                 @include('flash::message')
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <h1>{{$org->name}}</h1>
                @if($org->subscribers->contains(Auth::User()))
                {!! Form::open(['url' => '/organizations/settings/'.$org->id]) !!}

                {!! Form::select('Email', $Uemails ,$Demail, ['class' =>'select2','placeholder' => 'Please Select Identifire']) !!}
                <br><br>
                {!! Form::select('Phone', $Uphones ,$Dphone, ['class' =>'select2','placeholder' => 'Please Select Identifire']) !!}
                
                
                {{--{!! Form::select('Icash', $Uicashs ,$Dicash, ['class' =>'select2','placeholder' => 'Please Select Identifire']) !!}<br>--}}

                <br><br>
                {!! Form::select('Device', $Udevices ,$Ddevice, ['class' =>'select2','placeholder' => 'Please Select Device']) !!}
<br><br>
                {!! Form::submit('Update Settings' ,['class' =>'btn btn-info']) !!}

                {!! Form::close() !!}
@endif
                <br>

                {!! Form::open(array('url' => '/organizations/subscribe/'.$org->id)) !!}
                @if($org->subscribers->contains(Auth::User()))
                    {!! Form::submit('UnSubscribe' ,['class' =>'btn btn-warning']) !!}
                @else
                    {!! Form::submit('Subscribe' ,['class' =>'btn btn-primary']) !!}
                @endif

                {!! Form::close() !!}

                {!! Form::open(array('url' => '/organizations/block/'.$org->id)) !!}
                @if(Auth::User()->blackList->contains($org))
                    {!! Form::submit('UnBlock' ,['class' =>'btn btn-success']) !!}
                @else
                    {!! Form::submit('Block' ,['class' =>'btn btn-danger']) !!}
                @endif
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <script type="text/javascript">
     
        //$('select').select2('val', '');

        $('select').select2({
            allowClear: true,
            placeholder: {
                id: "-1",
                text:"Nothing selected"                
            }});
    </script>
@endsection
