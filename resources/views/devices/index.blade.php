@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Devices

                    <div class="panel-body">
                        <div class="box-body no-padding">
                            <table class="table table-striped">
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Label</th>
                                    <th>IMEI</th>
                                    <th style="width: 120px">Action</th>
                                </tr>
                                @forelse($devices as $k=>$device)
                                    <tr>
                                        <td>{{$k+1}}</td>
                                        <td>{{$device->label}}</td>
                                        <td>{{$device->imei_code}}</td>

                                        <td>
                                            <a href="/devices/{{$device->imei_code}}/edit" onclick="event.preventDefault(); document.getElementById('EDIT-Form-{{$k+1}}').submit();"><span class="label label-info"><i class="fa fa-edit"></i></span></a><form id="EDIT-Form-{{$k+1}}" action="{{ route('Edit-Device', $device->imei_code) }}" method="GET" style="display: none;">{{ csrf_field() }}</form>
                                            <a href="/devices/{{$device->label}}/delete" onclick="event.preventDefault(); document.getElementById('DELETE-Form-{{$k+1}}').submit();"><span class="label label-danger"><i class="fa fa-trash"></i></span></a><form id="DELETE-Form-{{$k+1}}" action="{{ route('Delete-Device', $device->id) }}" method="POST" style="display: none;">{{ csrf_field() }}{{ method_field('DELETE') }}</form>
                                            <span class="label label-danger"><i class="fa fa-trash"></i></span>
                                        </td>
                                    </tr>
                                @empty
                                    <p>No users</p>
                                @endforelse
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
