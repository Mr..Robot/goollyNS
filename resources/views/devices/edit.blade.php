@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit/Device
                    <div class="panel-body">
                        <form role="form" action="/devices/{{$device->id}}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="device">Label</label>
                                            <input type="text" class="form-control" value="{{$device->label}}"  name="device"  id="device" placeholder="Enter Label For Device" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="device">IMEI</label>
                                            <input type="number" disabled class="form-control" value="{{$device->imei_code}}"  name="imei_code"  id="imei_code" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
