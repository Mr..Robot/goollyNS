@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">identifiers/verify
                    </div>
                    {{--@include('flash::message')--}}
                    <div class="panel-body">
                        <form role="form" action="/activate" method="POST">
                            {{ csrf_field() }}
                            <div class="panel-body">
                                <p>Please enter the pin code we sent to you</p>
                                <div class="col-xs-4"></div>
                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <label for="pin">PIN CODE</label>
                                        <input  type="hidden" value="{{Request::route('login')}}" name="login">
                                        <input style="font-size: 48px; padding-left: 20px;" type="text" class="form-control input-lg" id="pin" width="80px" name="login_token" placeholder="######" value="{{Request::route('login_token') or '' }}">
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <button type="submit" class="btn btn-primary">Activate</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
