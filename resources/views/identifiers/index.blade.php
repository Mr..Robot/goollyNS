@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">identifiers
                        <div class="pull-right">
                            <a href="/identifiers/add"> <span class="label label-primary">New
                 		 <i class="fa fa-plus"></i></span></a></div></div>

                    <div class="panel-body">
                        <div class="box-body no-padding">
                            <table class="table table-striped">
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Type</th>
                                    <th>identifier</th>
                                    <th style="width: 40px">Status</th>
                                    <th style="width: 120px">Action</th>
                                </tr>
                                @forelse($ids as $k=>$id)
                                    <tr>
                                        <td>{{$k+1}}</td>
                                        <td>{{$id->type->name}}</td>
                                        <td>{{$id->value}}</td>
                                        @if($id->verified)
                                            <td><span class="label label-success">Activated</span></td>
                                        @elseif(!$id->verified)
                                            <td><span class="label label-danger">Deactivated</span></td>
                                        @endif
                                        <td>

                                            @if($id->verified)
                                                <a href="/identifiers/{{$id->value}}/Deactivate" onclick="event.preventDefault(); document.getElementById('DEVERIFY-Form-{{$k+1}}').submit();"><span class="label label-success"><i class="fa fa-times-circle-o"></i></span></a><form id="DEVERIFY-Form-{{$k+1}}" action="{{ route('DeVerifyID', $id->value) }}" method="POST" style="display: none;">{{ csrf_field() }}{{ method_field('PUT') }}</form>


                                            @elseif(!$id->verified)
                                                <a href="/identifiers/{{$id->value}}/Activate" onclick="event.preventDefault(); document.getElementById('VERIFY-Form-{{$k+1}}').submit();"><span class="label label-success"><i class="fa fa-check-circle-o"></i></span></a><form id="VERIFY-Form-{{$k+1}}" action="{{ route('RequestVerify', $id->value) }}" method="POST" style="display: none;">{{ csrf_field() }}
                                                </form>
                                            @endif

                                            <a href="/identifiers/{{$id->value}}/delete" onclick="event.preventDefault(); document.getElementById('DELETE-Form-{{$k+1}}').submit();"><span class="label label-danger"><i class="fa fa-trash"></i></span></a><form id="DELETE-Form-{{$k+1}}" action="{{ route('DeleteID', $id->value) }}" method="POST" style="display: none;">{{ csrf_field() }}{{ method_field('DELETE') }}</form>

                                             @if($id->default)
                                                <a href="/identifiers/{{$id->value}}/setDefualt" onclick="event.preventDefault(); document.getElementById('DEVERIFY-Form-{{$k+1}}').submit();"><span class="label label-warning"><i class="fa fa-star"></i></span></a><form id="DEVERIFY-Form-{{$k+1}}" action="{{ route('DeVerifyID', $id->value) }}" method="POST" style="display: none;">{{ csrf_field() }}{{ method_field('PUT') }}</form>


                                            @elseif(!$id->default)
                                                <a href="/identifiers/{{$id->value}}/setDefualt" onclick="event.preventDefault(); document.getElementById('VERIFY-Form-{{$k+1}}').submit();"><span class="label label-warning"><i class="fa fa-star-o"></i></span></a><form id="VERIFY-Form-{{$k+1}}" action="{{ route('RequestVerify', $id->value) }}" method="POST" style="display: none;">{{ csrf_field() }}
                                                </form>
                                            @endif


                                            <span class="label label-primary"><i class="fa fa-cogs"></i></span>
                                        </td>
                                    </tr>
                                @empty
                                    <p>No users</p>
                                @endforelse
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
