@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Identifires/add
                        <!--       	<div class="pull-right">
                                      <a href="/identifiers/add">
                                          <span class="label label-primary">New   <i class="fa fa-plus"></i></span>
                                      </a>
                                  </div> -->
                    </div>

                    <div class="panel-body">
                        <form role="form" action="/identifiers" method="POST">
                            {{ csrf_field() }}
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Type</label>
                                            <select class="form-control select2" name="type" style="width: 100%;">
                                                @foreach($types as $type)
                                                    <option value="{{$type->name}}">{{$type->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <label for="Identifire">Phone, Email, ICASH, etc..</label>
                                            <input type="text" class="form-control"  name="identifier"  id="Identifire" placeholder="Enter Identifire" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="verify"> Verify Now
                                    </label>
                                </div>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
