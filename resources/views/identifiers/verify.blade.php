@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">identifiers/verify
                    </div>
                    {{--@include('flash::message')--}}
                    <div class="panel-body">
                        <form role="form" action="/identifiers/verify" method="POST">
                            {{ csrf_field() }}
                            <div class="panel-body">
                                <p>Please enter the pin code we sent to your identifier</p>
                                <div class="col-xs-4"></div>
                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <label for="pin">PIN CODE</label>
                                        <input  type="hidden" value="{{$marker->value}}" name="identifier">


                                        <input style="font-size: 48px; padding-left: 20px;" type="text" class="form-control input-lg" id="pin" width="50px" name="pin" placeholder="####" value="{{ $Xcode or '' }}">
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <button type="submit" class="btn btn-primary">Verify</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
