<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
    //protected $dateFormat = 'Y-m-d H:i:s';
    protected $dateFormat = 'Y-m-d H:i:s.u';
        public function user()
    {
        return $this->belongsTo(User::class);
    }
}
