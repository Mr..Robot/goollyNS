<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detail extends Model
{
  //protected $dateFormat = 'Y-m-d H:i:s';
    protected $dateFormat = 'Y-m-d H:i:s.u';
}
