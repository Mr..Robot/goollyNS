<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Request extends Model
{
	//protected $dateFormat = 'Y-m-d H:i:s';
    protected $dateFormat = 'Y-m-d H:i:s.u';
    public function organization()
    {
        return $this->belongsTo(Organization::class);//->withTimeStamps();
    }

}
