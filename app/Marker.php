<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Marker extends Model
{
  //protected $dateFormat = 'Y-m-d H:i:s';
    protected $dateFormat = 'Y-m-d H:i:s.u';
    protected $primaryKey = 'value';
    public $incrementing = false;
    public $table = 'identifiers';

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function type(){
        return $this->belongsTo(Type::class);
    }
}
