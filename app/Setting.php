<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
  //protected $dateFormat = 'Y-m-d H:i:s';
  protected $dateFormat = 'Y-m-d H:i:s.u';
    public function identifier()
    {
        return $this->belongsTo(Identifier::class);//, 'identifier_id','id');
    }

    public function type(){
        return $this->belongsTo(Type::class);
    }

}
