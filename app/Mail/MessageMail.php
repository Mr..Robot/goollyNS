<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MessageMail extends Mailable
{
    use Queueable, SerializesModels;

    public $title;
    public $body;
    public $user;

    /**
     * Create a new message instance.
     *
     * @param $title
     * @param $body
     */
    public function __construct(User $user,$title, $body)
    {
        //
        $this->user = $user;
        $this->title = $title;
        $this->body = $body;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.message');
    }
}
