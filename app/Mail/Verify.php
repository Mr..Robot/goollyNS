<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Verify extends Mailable
{
    use Queueable, SerializesModels;


    public $Xcode;
    public $email;



    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($Xcode, $email)
    {
        //
        $this->Xcode = $Xcode;
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.verify');
    }
}
