<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
//  protected $dateFormat = 'Y-m-d H:i:s';
protected $dateFormat = 'Y-m-d H:i:s.u';
    public function organization(){
        return $this->belongsTo(Organization::class);
    }

    public function users(){
        return $this->belongsToMany(User::class)->withTimeStamps();
    }

    public function details(){
        return $this->hasMany(Detail::class);
    }

}
