<?php

namespace App\Helpers;

use App\Activation;

use App\Mail\MessageMail;
use App\Organization;
use App\User;
use App\Device;
use App\Token;
use App\Pending;
use Auth;



use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use LaravelFCM\Facades\FCM;

trait HelperFunctions
{
    public function refineNum($num)
    {
        $num = substr($num, -8, 8);
        $num = '2189' .$num;
        return $num;
    }
    public function checkNum($num)
    {
        if (starts_with($num, '218') or starts_with($num, '+218') or starts_with($num, '00218') or starts_with($num, '09'))
            return true;
        else
            return false;
    }
    public function isEmail($login)
    {
        if (filter_var($login, FILTER_VALIDATE_EMAIL))
            return true;
        else
            return false;
    }

    public function sendActivationEmail($Xcode,$idValue){



    }

    public function setDefaultId($type){    	
    	if(!Auth::User()->okIdentifiers()->where('type_id',$type)->where('default',true)->exists()){
    		$id = Auth::User()->okIdentifiers()->where('type_id',$type)->where('default',0)->get()->first();
    		if (!is_null($id))
    			$id->update(['default'=>1]);
    	}
    }


    public function sendPendings(User $user,Organization $organization){

    	$pendings = Auth::User()->pendings()->where('organization_id',$organization->id)->get();
    	
    	foreach ($pendings as $key => $pending) {
    		$this->sendPending($pending,$organization,$user);
    	}  
    }

    public function sendPending(Pending $pending,Organization $org, User $user)
    {
    	$identifier = $this->getDefaultID($user, $org,$pending->type);
    	if (!$identifier)
    		return 0;

    	switch ($pending->type) {

    		case 2: 
    		$this->smsMessage($identifier->value, $org->name, $pending->body);
    		break;

    		case 1:    		
    		 $this->emailMessage($user, $identifier->value, $org->name, $pending->body);
    		break;

    		case 3: $this->pushMessage($user, $org->name, $pending->body,$org);
    		break;
    	}
    	$pending->delete();
    }

    public function emailMessage(User $user, $email, $title, $body){
        \Mail::to($email)->send(new MessageMail($user,$title,$body));
        return true ;
    }

    public function smsMessage($number, $title, $body)
    {
        $sid = "ACaf6d514223be08ee0e07c337c4bf9dba";
        $token = "8fff0df8cdc8307508d9595ece603159";
        $client = new \Twilio\Rest\Client($sid, $token);
        $message = $client->messages->create('+'.$number, array('from' => "Goolly", 'body' => $body));
        return true;
    }

    public function pushRequest(User $user, $title, $org){
       

       if (!count($user->devices)) {
            return 0;
        }


        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);
        $notificationBuilder = new PayloadNotificationBuilder($title);
        $notificationBuilder->setBody($org->name .' Wants To contact you')->setSound('default')->setClickAction('requests');
        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        // You must change it to get your tokens
        $tokens = Device::where('user_id', $user->id)->pluck('fcm_token')->toArray();
        $downstreamResponse = FCM::sendTo($tokens, $option, $notification);
        return $downstreamResponse;
        //$downstreamResponse->numberSuccess();
        //$downstreamResponse->numberFailure(); 
        //$downstreamResponse->numberModification();
        //return Array - you must remove all this tokens in your database
        //$downstreamResponse->tokensToDelete(); 
        //return Array (key : oldToken, value : new token - you must change the token in your database )
        //$downstreamResponse->tokensToModify(); 
        //return Array - you should try to resend the message to the tokens in the array
        //$downstreamResponse->tokensToRetry();
        // return Array (key:token, value:errror) - in production you should remove from your database the tokens present in this array 
        //$downstreamResponse->tokensWithError(); 

    }

    public function pushMessage(User $user, $title, $body, $org)
    {
        //return $title;

        if (!count($org->devices()->where('device_settings.user_id',$user->id)->get())) {
        	flash('We cannot send your Push notification !!')->warning();
            return 0;
        }
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);
        $notificationBuilder = new PayloadNotificationBuilder($title);
        $notificationBuilder->setBody($body)->setSound('default')->setClickAction('messages');    
        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();            
        $device = $org->devices()->where('device_settings.user_id',$user->id)->first();
        $downstreamResponse = FCM::sendTo($device->fcm_token, $option, $notification);
        return $downstreamResponse;
        //$downstreamResponse->numberSuccess();
        //$downstreamResponse->numberFailure(); 
        //$downstreamResponse->numberModification();
        //return Array - you must remove all this tokens in your database
        //$downstreamResponse->tokensToDelete(); 
        //return Array (key : oldToken, value : new token - you must change the token in your database )
        //$downstreamResponse->tokensToModify(); 
        //return Array - you should try to resend the message to the tokens in the array
        //$downstreamResponse->tokensToRetry();
        // return Array (key:token, value:errror) - in production you should remove from your database the tokens present in this array 
        //$downstreamResponse->tokensWithError(); 
    }


    public function MakeActivator($idValue)
    {
        $Xcode = random_int(1111, 9999);
        $Activation = New Activation;
        $Activation->value = $idValue;
        $Activation->code = $Xcode;
        $Activation->save();

        return $Xcode;
    }
    public function getDefaultID(User $user,Organization $org, $type){

        if($user->settings()->where([['organization_id', '=', $org->id], ['type_id', '=', $type]])->exists()){
            $settings = $user->settings()->where([['organization_id', '=', $org->id], ['type_id', '=', $type]])->get()->first();
            return$settings->identifier;
        }else{
            return false;
        }

        //foreach ($settings as $set) {
          //  if ($set->type->name == $type) {
            //    return $set->identifier;
           // } else {
             //   return $this->loadUserDefaults($user,$type);
            //}
        //}

    }
    public function loadUserDefaults(User $user, $type){
        $identifiers = $user->okIdentifiers()->where('default',1)->get();

        foreach ($identifiers as $id){
            if($id->type->name == $type){
                return $id;
            }
        }
        return false;
    }
    public function response($errors,$code = 200){
        $transformed = [];
        $errors = json_decode(json_encode($errors), true);
        foreach ($errors as $field => $message) {
            $transformed[] = [
                'field' => $field,
                'message' => $message[0]
            ];
        }
        return response()->json(['error' => true,
            'msgs' => $transformed
        ], $code);
    }
    public function generateAuthToken(User $user){
        $token = new Token;
        $token->value = str_random(60);
        $user->tokens()->save($token);
        $user->api_token = $token->value;
        $user->save();
        return $user;
    }


    public function AuthUser(Request $request)
    {
        $token = Token::where('value',$request->input('api_token'))->first();              
        $user = $token->user;            
        return $user;

    }

    public function syncDevice(User $user,Request $request){

        $device = Device::where('imei_code', $request->imei_code)->first();
        if ($device == null) {
            $device = new Device;
            $device->label = 
            $device->imei_code = $request->imei_code;
            $device->fcm_token = $request->fcm_token;
            $user->devices()->save($device);
        }else{


        $device->fcm_token = $request->fcm_token;        
        $device->save();
    }
        return $device;

    }
}

