<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Identifier extends Model
{
  //protected $dateFormat = 'Y-m-d H:i:s';
    protected $dateFormat = 'Y-m-d H:i:s.u';
    public function user(){
        return $this->belongsTo(User::class);
    }

    public function type(){
        return $this->belongsTo(Type::class);
    }
    public function settings()
    {
        return $this->hasMany(Setting::class);
    }
    protected $guarded =[];

protected $casts = [
        'verified' => 'boolean',
		'default' => 'boolean',
		'type_id' => 'integer',
    ];

}
