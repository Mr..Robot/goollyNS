<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activation extends Model
{
  //protected $dateFormat = 'Y-m-d H:i:s';
    protected $dateFormat = 'Y-m-d H:i:s.u';
    protected $primaryKey = 'value';
    public $incrementing = false;
    public $timestamps = false;

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->created_at = $model->freshTimestamp();
        });
    }


}
