<?php
namespace App\Http\Middleware;
use Closure;
use App\Token;
use Carbon\Carbon ;

class AuthTokenChek
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->has('api_token')) {
            if (Token::where('value',$request->input('api_token'))->exists()) {
                $token = Token::where('value',$request->input('api_token'))->first();
                if ($this->chekExpiration($token))
                    return $next($request,compact('token'));
                else
                    return response()->json(['error' => true,'msg' => 'Api Token Expired']);
            }else{
                return response()->json(['error' => true,'msg' => "Api Token doesn't exist"]);
            }
        }else{
            return response()->json(['error' => true,'msg' => "Please login"]);
        }
    }

    public function chekExpiration(Token $token)
    {
        $now = Carbon::now();        
        if ($token->created_at->diffInDays($now) < 14)
            return true;
        else
            return false;
    }
}
