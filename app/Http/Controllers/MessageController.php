<?php

namespace App\Http\Controllers;


use App\Detail;
use App\Helpers\HelperFunctions;
use App\Marker;
use App\Organization;

use App\Type;
use Auth;
use App\Identifier;
use App\Request as Req;
use App\Message;
use App\Pending;
use Illuminate\Http\Request;

class MessageController extends BaseController
{
    use HelperFunctions;
    public function index()
    {
        $msgs = Auth::User()->messages;
        return view('messages.index',compact('msgs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $types = Type::all();
        return view('messages.create', compact('types'));
       // return view('');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function show(Message $message)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function edit(Message $message)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function sendMessage(Request $request, Message $message)
    {
       // return $request->all();
          $this->validate(request(), [
            'msg' => 'required',
            'identifier' => 'required',
            'sendon' => 'required',
        ]);
        $identifier = $request->identifier;
        if(!$this->isEmail($identifier))
            $identifier = $this->refineNum($identifier);

        $msg = New \App\Message;
        $msg->body = $request->msg;
        $marker = Marker::find($identifier);

        if (is_null($marker)) {
            flash("This user Doesn't exist")->error();
                return back()->withInput();
        }

        $user = $marker->user;

        $org = Auth::guard('org')->User();
        
        if ($user->blacklist->contains($org)) {
            flash('This user has Blocked your messages!!')->warning();
            return back()->withInput();      
        }


        $msg = $org->messages()->save($msg);

        $type = Type::where('name',$request->sendon)->get()->first();

        $user->messages()->attach($msg);

        if (!$org->subscribers->contains($user)) {

            $req = New Req;
            $req->organization_id = $org->id;
            $user->requests()->save($req);              
            $this->pushRequest($user, "Subscription Request", $org);

            $pending = new Pending;
            $pending->organization_id = $org->id;
            $pending->user_id = $user->id;
            $pending->body = $msg->body;
            $pending->type = $type->id;
            $pending->save();

            flash('Request Sent, Will get the message after request Accepted!')->warning();
            return back();
        }

        if($request->sendon =='Push' ) {
            if ($this->pushMessage($user, $org->name, $msg->body,$org))
                flash("Push was successfully sent")->success();
        }else{

        $identifier = $this->getDefaultID($user, $org, $type->id);


        if ($identifier == false) {
            flash('We cannot send your Message!!')->warning();
            return back();
        }

        switch ($request->sendon) {
            case 'Phone':
                if ($this->smsMessage($identifier->value, $org->name, $msg->body))
                    flash('SMS was successfully sent')->success();
                break;
            
            case 'Email': 
                if ($this->emailMessage($user, $identifier->value, $org->name, $msg->body))
                    flash("Email was successfully sent")->success();
                break;
                     
            default: flash('Not Available Type')->error();
        }
    }

        $details = New Detail;
        $details->identifier_id = $marker->id;
        $user->messages()->attach($msg);
        $msg->details()->save($details);

        return back();                        
            
            }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */



    public function destroy(Message $message)
    {
        //
    }


}
