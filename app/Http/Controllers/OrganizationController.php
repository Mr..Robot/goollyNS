<?php

namespace App\Http\Controllers;

use App\Organization;
use App\Type;
use Illuminate\Http\Request;
use Auth;
use App\Helpers\HelperFunctions;
class OrganizationController extends Controller
{
    use HelperFunctions;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $organizations = Organization::all();
        return view('organizations.index',compact('organizations'));
    }



    public function myOrgs()
    {
        $user = Auth::User();
        //return $user;
        $organizations = $user->myOrgs()->get();
        
        return view('organizations.index',compact('organizations'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('organizations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function createORG(Request $request)
    {
        if (Auth::check()) {                
            $login = $request->login;
        $isEmail = $this->isEmail($login);
        if (!$isEmail) {
            if ($this->checkNum($login)) {
                $login = $this->refineNum($login);
            } else {
                return response(['error' => true, 'msg' => 'Please enter valid phone number'], 200);
            }
        }



            $request->replace(array('login' => $login,
                                    'name'=>$request->name,
                                    'password'=>$request->password));

            $this->validate($request, [
            'name' => 'required|unique:organizations,name|max:255',
            'login' => 'required|unique:organizations,login|max:255',
            'password' => 'required|max:255,min:6',
           
        ]);

        $org = new Organization;
        $org->name = $request->name;
        $org->login = $login;
        $org->api_token =bcrypt($request->login);
        $org->password = bcrypt($request->password);
        $org->user_id = Auth::User()->id;
        $org->save();

        return redirect()->route('login-org');
    }else{
         flash('Please Login first !')->error();
        return redirect()->route('login');
    }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function show(Organization $organization)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function edit(Organization $organization)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Organization $organization)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function destroy(Organization $organization)
    {
        //
    }






    public function defualts($organization)
    {

        $user = Auth::User();
        $org = $organization = Organization::where('name', '=', $organization)->get()->first();

        $x = $user->okidentifiers->where('type_id',1);
        $Uemails = $x->pluck('value','id');
        $y = $user->okidentifiers->where('type_id',2);
        $Uphones = $y->pluck('value','id');
        $z = $user->okidentifiers->where('type_id',3);
        $Uicashs = $z->pluck('value','id');
        $Udevices = $user->devices()->pluck('label','id');

        $Demail = $organization->defualts->whereIn('id', $x->pluck('id'))->pluck('id');
        $Dphone = $organization->defualts->whereIn('id', $y->pluck('id'))->pluck('id');
        $Dicash = $organization->defualts->whereIn('id', $z->pluck('id'))->pluck('id');
        $Ddevice = $organization->devices->whereIn('id', $Udevices->pluck('id'))->pluck('id');

//return compact('org','Uemails','Uphones','Uicashs','Demail','Dphone','Dicash');
        return view('organizations.show',compact('org','Uemails','Uphones','Uicashs','Udevices','Demail','Dphone','Dicash','Ddevice'));

    }



    public function setDefults(Request $request,Organization $organization)
    {   
     
        list($keys, $values) = array_divide($request->only(['Phone', 'Email']));
        $array =[];
         $arr =[];

        foreach ($values as $k=>$value)
        {
            $type = Type::where('name',$keys[$k])->get()->first();

            $array = array_add($array, $value,
                [
                    'user_id'=>Auth::id(),
                    'type_id'=>$type->id
                ]);
        }

     $arr =array_add($arr,$request->Device,['user_id'=>Auth::ID()]);
        $organization->defualts()->sync($array);
     $organization->devices()->sync($arr);
        
        flash('success Updated')->success();
        return back();        
    }

    public function block(Organization $organization)
    {
        $user = Auth::User();

        $user->blacklist()->toggle($organization);

        $organization->subscribers()->detach($user);

         $user->requests()->where('organization_id',$organization->id)->update(['blocked' => 1,'seen'=>1]);

        return back();
    }


    public function subscribe(Organization $organization)
    {
        $user = Auth::User();


        $user->blacklist()->detach($organization);
        $organization->subscribers()->toggle($user);

        $user->requests()->where('organization_id',$organization->id)->update(['accepted' => 1,'seen'=>1]);

        if(!$organization->subscribers->contains($user)){
            $organization->defualts()->sync([]);
        }else{
            $this->setOrgUserSetting($organization);
            $this->sendPendings($user,$organization);
        }
        
        return back();

    }

    public function setOrgUserSetting(Organization $organization)
    {

        $ids = Auth::User()->okidentifiers->where('default',true);

        $array =[];

        foreach ($ids as $k=>$id)
        {                    
            $array = array_add($array, $id->id,
                [
                    'user_id'=>Auth::id(),
                    'type_id'=>$id->type_id
                ]);
        }    
        $organization->defualts()->sync($array);


        flash('success Updated')->success();
        return back();   
    }


    public function subs(){
        $user = Auth::User();
        $organizations = $user->organizations;
        return view('organizations.index',compact('organizations'));
    }

    public function blackList(){
        $user = Auth::User();
        $organizations = $user->blacklist;
        return view('organizations.index',compact('organizations'));
    }







//
//    public function defualts($organization)
//    {
//
//       // return $organization;
//        $user = Auth::User();
//        $org = Organization::where('name', '=', $organization)->get()->first();
//
//        $x = $user->okidentifiers->where('type_id',1);
//        $Uemails = $x->pluck('value','id');
//        $y = $user->okidentifiers->where('type_id',2);
//        $Uphones = $y->pluck('value','id');
//        $z = $user->okidentifiers->where('type_id',3);
//        $Uicashs = $z->pluck('value','id');
//
//        return $Demail = $org->userSettings;//->whereIn('id', $x->pluck('id'))->pluck('id');
//        $Dphone = $org->defualts->whereIn('identifier_id', $y->pluck('id'))->pluck('id');
//        $Dicash = $org->defualts->whereIn('identifier_id', $z->pluck('id'))->pluck('id');
//
//
//        return view('users.orgnazations.profile',compact('org','Uemails','Uphones','Uicashs','Demail','Dphone','Dicash'));
//
//    }

}
