<?php

namespace App\Http\Controllers;


use Validator;
use Auth;
use FCM;


use App\Marker;
use App\Organization;
use App\Identifier;
use App\Activation;
use App\Type;
use App\User;
use App\Detail;
use App\Device;
use App\Token;
use App\Request as Req;
use App\mail\Verify;
use App\mail\Message;
use App\Mail\Activate;
use App\Helpers\HelperFunctions;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;


class ApiController extends BaseController
{
    use HelperFunctions;

    public function register(Request $request)
    {
        $login = $request->login;

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:30',
            'login' => 'required|unique:users,login',
            'password' => 'required|min:6',
        ]);

        if ($validator->fails()) {
            return $this->response($validator->errors(), 200);
        }

        $check = $this->isEmail($login);

        if (!$check) {
            if ($this->checkNum($login)) {
                $login = $this->refineNum($login);
            } else {
                return response(['error' => true, 'msg' => 'Please enter valid phone number'], 200);
            }
        }

        $user = User::where("login", $login)->first();

        if (!is_null($user)) {
            if (!$user->active) {
                $user->delete();
                \DB::table('activations')->where('value', $login)->delete();
            } else {
                return response(['error' => true, 'msg' => 'this user already Exists'], 200);
            }
        }

        $user = New User;
        $user->name = $request->name;
        $user->login = $login;
        $user->password = bcrypt($request->password);
        $user->login_token = str_random(6);
        $user->api_token = str_random(60);
        if ($user->save()) {
            if ($check) {
                \Mail::to($login)->send(new Activate($user->login_token, $login));
            } else {
                $this->smsMessage($login, "Goolly NS", "This is Your Confirmation Code : " . $user->login_token);
            }
            return response(['error' => false, 'msg' => 'Successfully registered', 'user' => $user], 200);
        } else {
            return response(['error' => true, 'msg' => 'not registered'], 200);
        }

    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'login' => 'required',
            'password' => 'required|min:6',
        ]);

        if ($validator->fails()) {
            return $this->response($validator->errors(), 200);
        }

        $login = $request->login;
        $check = $this->isEmail($login);
        if (!$check) {
            if ($this->checkNum($login)) {
                $login = $this->refineNum($login);
            } else {
                return response(['error' => true, 'msg' => 'Please enter valid phone number'], 200);
            }
        }
        if (Auth::attempt(['login' => $login, 'password' => $request->password])) {

            $user = $this->generateAuthToken(Auth::User());

            $device = $this->syncDevice($user, $request);

            return response(['error' => false, 'user' => $user], 200);

        } else {
            return response(['error' => true, 'msg' => 'Please Check password and login'], 200);
        }
    }


    public function home(Request $request)
    {

        $requests = $this->AuthUser($request)->requests;
        $messages = $this->AuthUser($request)->messages()->with('organization')->get();

        return $this->AuthUser($request)->messages; //['requests' => $requests, 'messages' => $messages];

    }

    public function createIdentifier(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'identifier' => 'required|unique:identifiers,value|max:255',
            'type' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->response($validator->errors());
        }


        $type = Type::Where('name', $request->type)->get()->first();

        if ($request->type == 'icash') {
            $validator = Validator::make($request->all(), [
                'identifier' => 'numeric|max:10|min:9',]);
        } elseif ($request->type == 'phone') {
            $validator = Validator::make($request->all(), [
                'identifier' => 'numeric|min:8',]);

            $num = $request->identifier;

            if ($this->checkNum($num)) {
                $idValue = $this->refineNum($num);
            } else {
                return ['error' => true, 'msg' => 'Please enter valid phone number'];
            }

        } elseif ($request->type == 'email') {
            $validator = Validator::make($request->all(), [
                'identifier' => 'email',]);
            $idValue = $request->identifier;
        }
        if ($validator->fails()) {
            return $validator->errors();
        }

        $id = New Identifier;
        $id->user_id = $this->AuthUser($request)->id;
        $id->type_id = $type->id;
        $id->value = $idValue;
        $id->save();

        return ['error' => true, 'msg' => 'Identifier Saved Successfully', 'identifier' => $id];
    }

    public function verifyRequest(Request $request, Identifier $identifier)
    {

        if ($identifier->user_id == $this->AuthUser($request)->id) {

            \DB::table('activations')->where('value', $identifier->value)->delete();

            $Xcode = $this->MakeActivator($identifier->value);

            if ($identifier->type_id == 3) {

            } elseif ($identifier->type_id == 2) {
                $this->smsMessage($identifier->value, 'Goolly NS', 'Your Verification code is : ' . $Xcode);
                return ['error' => false, 'msg' => 'Check Your SMS'];

            } elseif ($identifier->type_id == 1) {
                \Mail::to($identifier->value)->send(new Verify($Xcode, $identifier->value));
                return ['error' => false, 'msg' => 'Check Your Email'];
            }

        } else {
            return $this->AuthUser($request)->id;
        }
    }


    public function verifyIdentifier(Request $request, Identifier $identifier)
    {

        $activation = Activation::findOrFail($identifier->value);

        if ($identifier->user_id == $this->AuthUser($request)->id) {
            if ($activation->code == $request->code) {
                $identifier->verified = true;
                $identifier->update();
                $activation->delete();
            } else {
                return ['error' => true, 'msg' => 'Wrong Code'];
            }
        } else {
            return "not yours";
        }

        return ['error' => false, 'msg' => 'Your Identifier "' . $identifier->value . '"was Successfully Activated '];
    }

    public function showOrg(Request $request, Organization $organization)
    {

        $user = $this->AuthUser($request);

        $org = $organization;// = Organization::where('name', '=', $organization)->get()->first();

        $x = $user->okidentifiers->where('type_id', 1);
        $Uemails = $x->pluck('value', 'id');
        $y = $user->okidentifiers->where('type_id', 2);
        $Uphones = $y->pluck('value', 'id');
        $z = $user->okidentifiers->where('type_id', 3);
        $Uicashs = $z->pluck('value', 'id');
        $Udevices = $user->devices()->pluck('label','id');

        $Demail = $organization->defualts->whereIn('id', $x->pluck('id'))->pluck('id');
        $Dphone = $organization->defualts->whereIn('id', $y->pluck('id'))->pluck('id');
        $Dicash = $organization->defualts->whereIn('id', $z->pluck('id'))->pluck('id');

        $Ddevice = $organization->devices->whereIn('id', $Udevices->pluck('id'))->pluck('id');
        
        $response = compact('org', 'Uemails', 'Uphones', 'Uicashs','Udevices', 'Demail', 'Dphone', 'Dicash','Ddevice');
        return $response;

    }


    public function showIdentifier(Request $request, Identifier $identifier)
    {

        if ($identifier->user_id == $this->AuthUser($request)->id) {
            return $identifier;
        } else {
            return "not yours";
        }


    }

    public function deverifyIdentifier(Request $request, Identifier $identifier)
    {

        if ($identifier->user_id == $this->AuthUser($request)->id) {
            $identifier->verified = false;
            $identifier->update();
            return ['error' => false, 'msg' => 'Your Identifier "' . $identifier->value . '"was Successfully Deactivated '];

        } else {
            return "not yours";
        }


    }

    public function deleteIdentifier(Request $request, Identifier $identifier)
    {
        if ($identifier->user_id == $this->AuthUser($request)->id) {
            $identifier->delete();
            return ['error' => false, 'msg' => 'Your Identifier "' . $identifier->value . '"was Successfully Deleted '];
        } else {
            return "not yours";
        }

    }

    public function ToggleActivation(Identifier $identifier)
    {
        // if ($identifier->verified) {
        //     $identifier->verified = false;
        // }else{
        //     $identifier->verified = true;
        // }

        $identifier->verified = !$identifier->verified;

        $identifier->update();
    }

    public function blockToggle(Request $request, Organization $organization)
    {
        $user = $this->AuthUser($request);

        $user->organizations()->detach($organization);
        \DB::table('settings')->where([['user_id', '=', $user->id], ['organization_id', '=', $organization->id]])->delete();

        if ($user->blacklist()->get()->contains($organization)) {
            $user->blacklist()->detach($organization);
            return response(['error' => false, 'msg' => 'Successfully UnBlocked', 'blocked' => false, 'followed' => false], 200);

        } else {

            $user->blacklist()->attach($organization);
            return response(['error' => false, 'msg' => 'Successfully Blocked', 'blocked' => true, 'followed' => false], 200);
        }
    }

    public function subscribeToggle(Request $request, Organization $organization)
    {

        $user = $this->AuthUser($request);

        if ($user->organizations()->get()->contains($organization)) {

            $user->organizations()->detach($organization);
            \DB::table('settings')->where([['user_id', '=', $user->id], ['organization_id', '=', $organization->id]])->delete();

            return response(['error' => false, 'msg' => 'Successfully UnFollowed', 'followed' => false, 'blocked' => false], 200);

        } else {
            $user->blacklist()->detach($organization);
            $array = [];

            if ($email = $this->loadUserDefaults($user, 'Email')) {
                $array = array_add($array, $email->id,
                    [
                        'user_id' => $user->id,
                        'type_id' => '1'
                    ]);
            }
            if ($phone = $this->loadUserDefaults($user, 'Phone')) {
                $array = array_add($array, $phone->id,
                    [
                        'user_id' => $user->id,
                        'type_id' => '2'
                    ]);
            }

            $user->organizations()->attach($organization);
            $organization->defualts()->sync($array);
            return response(['error' => false, 'msg' => 'Successfully Followed', 'followed' => true, 'blocked' => false], 200);
        }

    }

    public function changeSettings(Request $request, Organization $organization)
    {


        list($keys, $values) = array_divide($request->only(['Phone', 'Email']));
        $array = [];
        $arr =[];

$arr =array_add($arr,$request->Device,['user_id'=>Auth::ID()]);
        foreach ($values as $k => $value) {
            $type = Type::where('name', $keys[$k])->get()->first();

            $array = array_add($array, $value,
                [
                    'user_id' => $this->AuthUser($request)->id,
                    'type_id' => $type->id
                ]);
        }
        $array;

        $organization->defualts()->sync($array);
        $organization->devices()->sync($arr);
        return response(['error' => false, 'msg' => 'Successfully Updated', 'updated' => true, 'followed' => true, 'blocked' => false], 200);

         


     
        
     






        //return $organization;
    }

    public function sendMessage(Request $request)
    {

        $identifier = $request->identifier;

        if (!$this->isEmail($identifier))
            $identifier = $this->refineNum($identifier);

        $msg = New \App\Message;

        $msg->body = $request->msg;

        $marker = Marker::find($identifier);

        $user = $marker->user;

        $org = $request->user('api-org'); //  Auth::guard('api-org')->User();


        $msg = $org->messages()->save($msg);

        $type = Type::where('name', $request->sendon)->get()->first();

        $user->messages()->attach($msg);

        $details = New Detail;
        $details->identifier_id = $marker->id;
        $user->messages()->attach($msg);
        $msg->details()->save($details);

        if ($user->blacklist->contains($org))
            return ["error" => true, "msg" => "This user has Blocked your messages!!"];

        if ($org->subscribers->contains($user)) {
            if ($request->sendon == 'Phone') {


                $identifier = $this->getDefaultID($user, $org, $type->id);

                if ($identifier == false) {
                    return ["error" => true, "msg" => "this user doesn't have a valid phone number"];
                }

                if ($this->smsMessage($identifier->value, $org->name, $msg->body)) {
                    return ["error" => false, "msg" => "SMS was successfully sent"];
                }

            } elseif ($request->sendon == 'Email') {

                $identifier = $this->getDefaultID($user, $org, $type->id);

                if ($identifier == false) {
                    return ["error" => true, "msg" => "this user doesn't have a valid email Address"];
                }
                if ($this->emailMessage($user, $identifier->value, $org->name, $msg->body)) {
                    return ["error" => false, "msg" => "Email was successfully sent"];
                }

            } elseif ($request->sendon == 'Push') {

                $this->pushMessage($user, $org->name, $msg->body);

            } else {

                return ["error" => true, "msg" => "Method Not Available"];
            }

        } else {

            //  $body =  $org->name . ' Wants To contact you ';

            $req = New Req;
            $req->organization_id = $org->id;

            $user->requests()->save($req);
            return $req;

            $this->pushRequest($user, "Subscription Request", $org);
            return ["error" => false, "msg" => "Contact request has been Sent"];

        }

    }

    public function getDevices(Request $request)
    {
        $user = $this->AuthUser($request);
        return $user->devices()->get();
    }

    public function getDevice(Request $request, Device $device)
    {
        return $device;
    }

    public function deleteDevice(Request $request, Device $device)
    {
        $user = $this->AuthUser($request);
        if ($user->devices->contains($device)) {
            \DB::table('device_settings')->where('device_id', $device->id)->delete();
            if ($device->delete())
                return ["error" => false, "msg" => "Device successfully Deleted, you can no longer receive notification!"];
            else
                return ["error" => true, "msg" => "Something went wrong, the device could not be Deleted, try again!"];
        } else {
            return ["error" => true, "msg" => "This Device is not yours"];
        }
    }

    public function editDevice(Request $request, Device $device)
    {
        $device->label = $request->label;
        $device->fcm_token = $request->fcm_token;
        $device->save();
        return ["error" => false, "msg" => 'Device "' . $device->label . '" Successfully Synced'];

    }

    public function addDevice(Request $request)
    {

        $device = Device::where("imei_code", $request->imei_code)->first();
        if ($device == null) {
            $user = $this->AuthUser($request);
            $device = New Device;
            $device->label = $request->label;
            $device->imei_code = $request->imei_code;
            $device->fcm_token = $request->fcm_token;
            if ($user->devices()->save($device)) {
                return ["error" => false, "msg" => "this device was add successfully!"];
            } else {
                return ["error" => true, "msg" => "this device can't be add"];
            }
        } else {
            return ["error" => false, "msg" => "This Device already Exists"];
        }


    }

    public function activateAccount(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'login' => 'required',
            'login_token' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->response($validator->errors());
        }

        $login = $request->login;
        $check = $this->isEmail($login);
        $type = 1;
        if (!$check) {
            if ($this->checkNum($login)) {
                $type = 2;
                $login = $this->refineNum($login);
            } else {
                return ['error' => true, 'msg' => 'Please enter valid Login'];
            }
        }

        $user = User::where("login", $login)->first();

        if ($user->login_token == $request->login_token) {
            $user->active = true;
            $user->login_token = null;
            $user->update();

            $id = New Identifier;
            $id->user_id = $user->id;
            $id->type_id = $type;
            $id->verified = 1;
            $id->default = 1;
            $id->value = $login;
            $id->save();
            return ['error' => false, 'msg' => 'Your Account Successfully Activated '];
        } else {
            return ['error' => true, 'msg' => 'Something went wrong try again'];
        }
    }


    public function orgLogin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'login' => 'required',
            'password' => 'required|min:6',
        ]);
        if ($validator->fails()) {
            return $this->response($validator->errors());
        }

        $login = $request->login;
        $check = $this->isEmail($login);
        if (!$check) {
            if ($this->checkNum($login)) {
                $login = $this->refineNum($login);
            } else {
                return ['error' => true, 'msg' => 'Please enter valid Login'];
            }
        }

        if (Auth::guard('org')->attempt(['login' => $login, 'password' => $request->password])) {
            $org = Organization::where([['login', '=', $login]])->get()->first();
            $org->update(['api_token' => str_random(60)]);
            return ['error' => false, 'api_token' => $org->api_token];
        } else {
            return ['error' => true, 'msg' => 'Please Check password and login'];
        }
    }

    public function user(Request $request)
    {

        return $this->AuthUser($request);

    }

    public function userMSGS(Request $request)
    {

        return $this->AuthUser($request)->messages()->with('organization')->get();

    }

    public function userORGS(Request $request)
    {

        $user = $this->AuthUser($request);

        $allORGs = $user->organizations()->get();

        $multiplied = $allORGs->map(function ($org) use ($user) {

            if ($user->organizations->contains($org))
                $org->followed = true;
            else
                $org->followed = false;

            if ($user->blacklist->contains($org))
                $org->blocked = true;
            else
                $org->blocked = false;

            return $org;

        });

        return $multiplied;


    }

    public function userIDENTIFIERS(Request $request)
    {

        return $this->AuthUser($request)->identifiers()->with('type')->get();
    }

    public function userBLACKLIST(Request $request)
    {

        $user = $this->AuthUser($request);

        $allORGs = $user->blacklist()->get();

        $multiplied = $allORGs->map(function ($org) use ($user) {

            if ($user->organizations->contains($org))
                $org->followed = true;
            else
                $org->followed = false;

            if ($user->blacklist->contains($org))
                $org->blocked = true;
            else
                $org->blocked = false;

            return $org;

        });

        return $multiplied;

    }

    public function ORGS(Request $request)
    {

        $user = $this->AuthUser($request);

        $allORGs = Organization::get();

        $multiplied = $allORGs->map(function ($org) use ($user) {

            if ($user->organizations->contains($org))
                $org->followed = true;
            else
                $org->followed = false;

            if ($user->blacklist->contains($org))
                $org->blocked = true;
            else
                $org->blocked = false;

            return $org;

        });

        return $multiplied;
    }

    
    public function getRequests(Request $request){
        $user = $this->AuthUser($request);
        $allReq = $user->requests()->where([['accepted','=','0'],['blocked','=','0']])->with('organization')->get();
        return $allReq;
    }

        public function blockRequest(Request $request,Req $req){
            $user = $this->AuthUser($request);
            $org = $req->organization;
            $req->blocked = 1;
            if($req->save())
                $this->blockToggle($request, $org);
            else
                return response(['error' => true, 'msg' => 'Something went wrong'], 200);

            
            return response(['error' => false, 'msg' => 'Request Successfully Blocked'], 200);
        }

        public function acceptRequest(Request $request,Req $req){
            $user = $this->AuthUser($request);
            $org = $req->organization;
            $req->accepted = 1;            
            if($req->save())
                $this->subscribeToggle($request, $org);
            else
                return response(['error' => true, 'msg' => 'Something went wrong'], 200);

            return response(['error' => false, 'msg' => 'Request Successfully Accepted'], 200);
        }
    
    

}