<?php

namespace App\Http\Controllers;
use App\Helpers\HelperFunctions;
use App\Mail\Verify;
use Auth;
use App\Identifier;
use App\Marker;
use App\Activation;
use App\Type;
use Illuminate\Http\Request;


class IdentifierController extends BaseController
{
    use HelperFunctions;
    public function index()
    {
        $user = Auth::User();
        $ids = $user->identifiers;
        //  dd($ids);
        return view('identifiers.index', compact('ids'));
    }

    public function create()
    {
        $types = Type::all();
        return view('identifiers.create', compact('types'));
    }

    public function store(Request $request)
    {
		$this->validate($request, [
            'identifier' => 'required|max:255',
            'type' => 'required',
        ]);
		
		if(Identifier::where('value', $request->identifier)->exists())
		{
			$identifier = Identifier::where('value', $request->identifier)->get()->first();
			if($identifier->verified){
				$this->validate($request, ['identifier' => 'unique:identifiers,value',]);				
			}else{
				$identifier->delete();			
			}
		}
		
        $type = Type::Where('name', $request->type)->get()->first();

        if ($request->type == 'Icash') {
            return $request->identifier;
            $this->validate($request, [
                'identifier' => 'numeric|max:10|min:9',]);
        } elseif ($request->type == 'Phone') {
            $this->validate($request, [
                'identifier' => 'numeric|min:8',]);

            $num = $request->identifier;
            if ($this->checkNum($num)) {
                $idValue = $this->refineNum($num);
            } else {
                return ['error' => true, 'msg' => 'Please enter valid phone number'];
            }

        } elseif ($request->type == 'Email') {
            $this->validate($request, [
                'identifier' => 'email',]);
            $idValue = $request->identifier;
        }

        $id = New identifier;
        $id->user_id = Auth::id();
        $id->type_id = $type->id;        
        $id->value = $idValue;
        $id->save();
  
        if (isset($request->verify) && !empty($request->verify)) {
			$Xcode = $this->MakeActivator($idValue);
            if ($request->type == 'Phone') {
                $this->smsMessage($idValue, "Goolly NS", 'Your Verification code is : ' . $Xcode);
            } elseif ($request->type == 'Email') {
                \Mail::to($idValue)->send(new Verify($Xcode, $idValue));
            }
            return redirect()->route('VerifyID', ['marker' => $idValue]);
        } else {
            return redirect()->route('IDList');
        }

    }
    public function verifyRequest(Marker $marker)
    {
        $Xcode = $this->MakeActivator($marker->value);

        if ($marker->user_id == Auth::ID()) {

            if ($marker->type_id == 3) {

            } elseif ($marker->type_id == 2) {
                $this->smsMessage($marker->value,'Goolly NS','Your Verification code is : ' . $Xcode);
                return redirect()->route('VerifyID', ['marker' => $marker->value]);

            } elseif ($marker->type_id == 1) {
                $this->sendActivationEmail($marker->value);
                return redirect()->route('VerifyID', ['marker' => $marker->value]);
            }

        } else {
            return Auth::ID();
            //  return "not yours";
        }
    }


    public function verify(Request $request, Marker $marker)
    {
        //$identifier->$primaryKey = 'value';
        return view('identifiers.verify', compact('marker'));
    }

    public function verifyEmail(Request $request, Marker $marker, $Xcode)
    {
        // return $request;

        return view('identifiers.verify', compact('marker', 'Xcode'));
    }


    public function update(Request $request)
    {
      //  return $request->all();

        $this->validate($request, [
            'identifier' => 'required|exists:identifiers,value',
            'pin' => 'required|exists:activations,code',
        ]);



        $activation = Activation::findOrFail($request->identifier);
        //   return $activation;
        $marker = Marker::findOrFail($request->identifier);
        // return $marker;
        if ($marker->user_id == Auth::ID()) {
            $this->ToggleActivation($marker);
            $activation->delete();
        } else {
            return Auth::ID();
            //return "not yours";
            
        }

        return redirect()->route('IDList');

    }


    public function destroy(Marker $marker)
    {
        if ($marker->user_id == Auth::ID()) {
            $marker->delete();

        } else {
            return Auth::ID();
            //return "not yours";
        }

        return redirect()->route('IDList');
    }

    public function deVerify(Marker $marker)
    {
        if ($marker->user_id == Auth::ID()) {
            $this->ToggleActivation($marker);
        } else {
            return Auth::ID();
            //  return "not yours";
        }

        return redirect()->route('IDList');
    }

    public function ToggleActivation(Marker $marker)
    {
        $user = $marker->user;    
        if ($marker->verified) {
            $marker->verified = false;
            $marker->default = false;                                
            $user->settings()->where('identifier_id',$marker->id)->delete();

        } else {
            $marker->verified = true;
            // if(!Identifier::where('user_id', Auth::ID())->where('default',true)->where('type_id',$marker->type_id)->exists()){
            //     $marker->default = true;                            
            //}
        }
        $marker->update();
       $this->setDefaultId($marker->type_id);
        return redirect()->route('IDList');
    }

    public function setDefault(Marker $marker){
        //$user = $marker->user;
        //$marker->update();
        //$this->setDefaultId($marker->type_id);
        return redirect()->route('IDList');
    }

    public function sendActivationEmail($idValue)
    {
        $Xcode = $this->MakeActivator($idValue);
        \Mail::to($idValue)->send(new Verify($Xcode, $idValue));
    }


}
