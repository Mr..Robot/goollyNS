<?php

namespace App\Http\Controllers;

use App\Identifier;
use App\Mail\Activate;
use App\Type;
use App\User;
use App\Helpers\HelperFunctions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends BaseController
{
    use HelperFunctions;

    public function ShowLoginForm()
    {
        return view('Auth.login');
    }

    public function ShowRegistrationForm()
    {
        return view('Auth.register');
    }

    public function ShowResetForm()
    {
        return view('Auth.reset');
    }

    public function ShowConfirmForm(){
        return view('Auth.confirm');
    }
    public function PostConfirm(Request $request){

        //return $request->all();
        $this->validate(request(), [
            'login' => 'required',
            'login_token' => 'required',
        ]);
        $type = Type::find(1);

        $login = $request->login;
        $check = $this->isEmail($login);
        if (!$check) {
            $type = Type::find(2);
            if ($this->checkNum($login)) {
                $login = $this->refineNum($login);
            } else {
                return ['error' => true, 'msg' => 'Please enter valid phone number'];
            }
        }

        $user = User::where("login",$login)->first();
        //return $user;


        if ($user->login_token == $request->login_token) {
            $user->active=true;
            $user->login_token = null;
            $user->update();

          Auth::login($user);

            $id = New Identifier;
            $id->user_id = Auth::id();
            $id->type_id = $type->id;
            $id->verified = 1;
            $id->default = 1;
            $id->value = $login;
            $id->save();
            return redirect()->route('IDList')->with('status', 'Your Account Is now Activated, Have Fun!');


        }else{
            return ['error' => true, 'msg' => 'Something went wrong try again'];}
    }


    public function PostLogin(Request $request)
    {
        $login = $request->login;
        $check = $this->isEmail($login);
        if (!$check) {
            if ($this->checkNum($login)) {
                $login = $this->refineNum($login);
            } else {
                return ['error' => true, 'msg' => 'Please enter valid phone number'];
            }
        }

        $request->replace(array('login' => $login,'password'=>$request->password));

        //return $request->all();
        $this->validate(request(), [
            'login' => 'required|exists:users,login',
            'password' => 'required',
        ]);
        
       $user = User::where('login', $login)->get()->first();

       // return $user;

        if ($user->active) {
            if (Auth::attempt(['login' => $request->login, 'password' => $request->password] ))
                return redirect('/');//->route('User.Activate', ['login'=> $login]);
             //   return Auth::user();
            else
                return "nooo";
                //return redirect()->intended(route('home'));
        }else{
            return redirect()->route('User.Activate', ['login'=> $login]);
        }

    }

    public function PostRegistration(Request $request)
    {
        $this->validate(request(), [
            'name' => 'required',
            'login' => 'required|unique:users,login',
            'password' => 'required|confirmed|min:6',
        ]);

        $login = $request->login;
        $check = $this->isEmail($login);
        if (!$check) {
            if ($this->checkNum($login)) {
                $login = $this->refineNum($login);
            } else {
                return ['error' => true, 'msg' => 'Insert Correct number '];
            }
        }

        $user = New User;
        $user->name = $request->name;
        $user->login = $login;
        $user->password = bcrypt($request->password);
        $user->login_token = str_random(6);
        $user->api_token = str_random(60);

        if ($user->save()) {
            if ($check)
                \Mail::to($login)->send(new Activate($user->login_token, $login));
            else
                $this->smsMessage($login, "Goolly NS", "This is Your Confirmation Code : " . $user->login_token);

            return redirect()->route('User.Activate', ['login'=> $login]);
        } else {
            return redirect(route('Login'))->with('status', 'User not created.');
        }
    }

    public function activate(Request $request){

        return $request->all();
    }
    public function logout(Request $request)
    {
        Auth::guard()->logout();

        $request->session()->invalidate();

        return redirect('/');
    }


}