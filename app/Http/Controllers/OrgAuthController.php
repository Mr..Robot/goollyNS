<?php

namespace App\Http\Controllers;

use App\Helpers\HelperFunctions;
use App\Organization;
use App\User;
use Illuminate\Http\Request;
use Auth;

class OrgAuthController extends BaseController
{

    use HelperFunctions;


    public function ShowLoginForm()
    {
        return view('Auth.organization.login');
    }
    public function PostLogin(Request $request)
    {
        Auth::guard('org')->logout();

        $login = $request->login;
        $check = $this->isEmail($login);
        if (!$check) {
            if ($this->checkNum($login)) {
                $login = $this->refineNum($login);
            } else {
                return ['error' => true, 'msg' => 'Please enter valid phone number'];
            }
        }



            $request->replace(array('login' => $login,
                                    'name'=>$request->name,
                                    'password'=>$request->password));


        //return $request->all();
        $this->validate(request(), [
            'login' => 'required|exists:organizations,login',
            'password' => 'required',
        ]);


       

        $user = Organization::where('login', $login)->get()->first();
        //   return $user;



        if ($user->active) {
            if (Auth::guard('org')->attempt(['login' => $login, 'password' => $request->password] ))
                return redirect('/messages/send');
               // return Auth::guard('org')->user();
            else
                return "nooo";
            //return redirect()->intended(route('home'));
        }else{
            return redirect()->route('User.Activate', ['login'=> $login]);
        }

    }
}
