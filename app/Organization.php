<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Organization extends Authenticatable
{
    use Notifiable;
  //protected $dateFormat = 'Y-m-d H:i:s';
protected $dateFormat = 'Y-m-d H:i:s.u';

    protected $guard = 'org';
    protected $fillable = [
        'name', 'email', 'password','api_token','active',
    ];

    protected $hidden = [
        'password', 'remember_token','user_id','api_token','pivot',
    ];

    public function messages(){
        return $this->hasMany(Message::class);
    }
    public function subscribers()
    {
        return $this->belongsToMany(User::class)->withTimeStamps();
    }

    public function userSettings()
    {
        return $this->hasManyThrough(User::class,Setting::class);
        //   return $this->hasManyThrough();
    }

    public function defualts()
    {
        return $this->belongsToMany(Identifier::class,'settings','organization_id','identifier_id')->withTimeStamps();
    }

    public function devices()
    {
        return $this->belongsToMany(Device::class,'device_settings','organization_id','device_id')->withTimeStamps();
    }




    public function defualtset()
    {
        return $this->belongsToMany(Identifier::class,'settings','organization_id','identifier_id')->withTimeStamps();
    }

    static public function findByToken($token,$secret)
    {
        $org = new Orgnazation;

        $org  = Orgnazation::where([
            ['token', '=', $token],
            ['secret', '=', $secret],
        ])->get();
        return $org;
    }

	protected $casts = [
        'active' => 'boolean',		
    ];

}
