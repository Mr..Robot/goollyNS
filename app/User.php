<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;    

    //protected $dateFormat = 'Y-m-d H:i:s';
    protected $dateFormat = 'Y-m-d H:i:s.u';
    protected $fillable = [
        'name', 'login', 'password','api_token','user_id', 'login_token',
    ];

    protected $hidden = [
        'password', 'remember_token','pivot',
    ];
    

    public function messages()
    {
        return $this->belongsToMany(Message::class)->withTimeStamps();
    }

    public function organizations()
    {
        return $this->belongsToMany(Organization::class)->withTimeStamps();
    }

    public function blacklist()
    {
        return $this->belongsToMany(Organization::class,"blackList")->withTimeStamps();
    }

    public function pendings()
    {
        return $this->hasMany(Pending::class);
    }

    public function identifiers()
    {
        return $this->hasMany(Identifier::class);
    }
    public function requests()
    {
        return $this->hasMany(Request::class);
    }

    public function myOrgs()
    {
        return $this->hasMany(Organization::class);
    }

    public function settings()
    {
        return $this->hasMany(Setting::class);
    }
    public function devices()
    {
        return $this->hasMany(Device::class);
    }


    public function okIdentifiers()
    {
        return $this->hasMany(Identifier::class)->where('verified',1);
    }


    public function tokens()
    {
        return $this->hasMany(Token::class);
    }

    public function requets()
    {
        return $this->hasMany(Request::class);
    }




    public function defualts()
    {
        return $this->hasMany(Defualt::class);
    }



    static public function findByIdentifier(Identifier $Identifier)
    {
        return $Identifier->user;
    }


}
