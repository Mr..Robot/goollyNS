<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
})->name('home');


Route::get('/logout', 'AuthController@Logout')->name('logout');//view

Route::get('organizations/login', 'OrgAuthController@ShowLoginForm')->name('login-org');//view
Route::post('organizations/login', 'OrgAuthController@postLogin')->name('login-org-post');

Route::post('organizations/register-org', 'organizationController@createORG')->name('create_org');

Route::get('/login', 'AuthController@ShowLoginForm')->name('login');//view
Route::get('/register', 'AuthController@ShowRegistrationForm')->name('register');//view
Route::get('/activate/{login?}', 'AuthController@ShowConfirmForm')->name('User.Activate');//view;
Route::get('/activate/{login?}/{login_token?}', 'AuthController@ShowConfirmForm')->name('User.Activate.email');//view;

Route::get('/password/reset', 'AuthController@ShowResetForm')->name('password.request');//view;

Route::post('/login', 'AuthController@postLogin');
Route::post('/register', 'AuthController@postRegistration');
Route::post('/password/reset', 'AuthController@postReset');

Route::post('/activate', 'AuthController@PostConfirm');

//Route::get('/home', 'HomeController@index')->name('home');//view

Route::get('/identifiers', 'identifierController@index')->name('IDList');////view

Route::get('/identifiers/add', 'identifierController@create')->name('CreateID');//view

Route::post('/identifiers', 'identifierController@store')->name('StoreID');//method

Route::get('/identifiers/{marker}/verify', 'identifierController@verify')->name('VerifyID');//view/phone

Route::get('/identifiers/{marker}/verify/{Xcode?}', 'identifierController@verifyEmail')->name('verifyIDEmail');//view/Email

Route::post('/identifiers/verify', 'identifierController@update')->name('UpdateID');//method

Route::delete('/identifiers/{marker}', 'identifierController@destroy')->name('DeleteID');//method

Route::put('/identifiers/{marker}', 'identifierController@ToggleActivation')->name('DeVerifyID');//method

Route::post('/identifiers/{marker}', 'identifierController@verifyRequest')->name('RequestVerify');//method

Route::post('/identifiers/{marker}/setDefault', 'identifierController@setDefault')->name('DefaultID');//method

Route::get('/messages', 'MessageController@index')->name('MSGList');

//name('ToggleSubscription');
Route::post('/blockList/{organization}', 'UserController@block');

Route::get('/blacklist', 'UserController@blockList');

Route::get('/devices', 'DeviceController@index');
Route::get('/devices/{imei?}', 'DeviceController@Edit')->name('Edit-Device');
Route::put('/devices/{device}', 'DeviceController@update');
Route::delete('/devices/{device}', 'DeviceController@destroy')->name('Delete-Device');
Route::get('/organizations', 'organizationController@index');

Route::get('/subscriptions', 'organizationController@subs');
Route::get('/blacklist', 'organizationController@blackList');
Route::get('/myorgs', 'organizationController@myOrgs');
Route::get('/organizations/create', 'organizationController@create');
Route::get('/organizations/{organization}', 'organizationController@defualts');

Route::post('/organizations/settings/{organization}', 'organizationController@setDefults');

Route::post('/organizations/subscribe/{organization}', 'organizationController@subscribe');//->
Route::post('/organizations/block/{organization}', 'organizationController@block');//



Route::get('/messages/send', 'MessageController@create');
Route::post('/messages/send', 'MessageController@sendMessage');