<?php
use App\Organization;
use App\Identifier;
use App\Type;
use Illuminate\Http\Request;


Route::post('/organizations/login', 'ApiController@orgLogin');

Route::group(['middleware' => ['auth:api-org']], function () {
    Route::post('/messages/send/', 'ApiController@sendMessage');
});

Route::post('/users/register', 'ApiController@register');
Route::post('/users/activate', 'ApiController@activateAccount');
Route::post('/users/login', 'ApiController@login');

Route::group(['middleware' => ['auth.token']], function () {

    Route::get('/home', 'ApiController@home');
    Route::get('/user','ApiController@user');
    Route::get('/user/messages', 'ApiController@userMSGS');
    
    Route::get('/user/blacklist', 'ApiController@userBLACKLIST');
    
    Route::get('/user/organizations', 'ApiController@userORGS');

    Route::get('/organizations', 'ApiController@ORGS');
    
    Route::get('/organizations/{organization}', 'ApiController@showOrg');
    
    Route::post('/organizations/{organization}/sub', 'ApiController@subscribeToggle');

    Route::post('/organizations/{organization}/block', 'ApiController@blockToggle');
    
    Route::post('/organizations/{organization}/update', 'ApiController@changeSettings');

    Route::get('/user/identifiers', 'ApiController@userIDENTIFIERS');
    Route::get('/user/identifiers/{identifier}', 'ApiController@showIdentifier');
    Route::post('/user/identifiers', 'ApiController@createIdentifier');
    Route::post('/user/identifiers/{identifier}', 'ApiController@verifyRequest');
    Route::post('/user/identifiers/{identifier}/verify', 'ApiController@verifyIdentifier');
    Route::post('/user/identifiers/{identifier}/deverify', 'ApiController@deverifyIdentifier');
    
    Route::post('/user/identifiers/{identifier}/delete', 'ApiController@deleteIdentifier');
    
    //Route::post('/user/blacklist', 'ApiController@');
    Route::get('/user/devices', 'ApiController@getDevices');
    Route::get('/user/devices/{device}', 'ApiController@getDevice');
    Route::post('/user/devices', 'ApiController@addDevice');
    Route::post('/user/devices/{device}', 'ApiController@editDevice');
    Route::post('/user/devices/{device}/delete', 'ApiController@deleteDevice');


    Route::get('/user/requests', 'ApiController@getRequests');
    Route::post('/user/requests/{req}/block', 'ApiController@blockRequest');
    Route::post('/user/requests/{req}/accept', 'ApiController@acceptRequest');





});

Route::group(['middleware' => ['auth:org-api']], function () {
    Route::post('/messages/send/', 'ApiController@sendMessage');

});
